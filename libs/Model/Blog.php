<?php 

namespace Model; 

class Blog extends \Emagid\Core\Model {

	static $tablename = 'blog'; 
	
	public static $fields =  [
		'title', 
		'slug',
		'description', 
		'featured_image',
		'meta_title', 
		'meta_keywords', 
		'meta_description',
		'date_modified'
	];
	
}