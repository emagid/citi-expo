<?php

namespace Model;

class Vote extends \Emagid\Core\Model {
    public static $tablename = "votes";

    public static $fields = [
        'fav_booth',
    ];
}