<?php

namespace Notification;

abstract class NotificationsHandler
{
  protected $message = '';
  protected $html = '';
  
  /**
   * 
   * Abstract function to build the output HTML
   * 
   */
  protected abstract function buildHTML();
  
  
  /**
   * 
   * Construct the error object
   * 
   */
  public function __construct($message)
  {
    $this->message = $message;
  }
  
  /**
   * 
   * Accessor get the HTML of the notification
   * 
   */
  public function getHTML()
  {
    return $this->html;
  }
  
  
  /**
   * 
   * Display the notification message
   * 
   */
  public function display()
  {
    echo "<div class='notification'>" . $this->html . "</div>";
  }
  
  
  /**
   * 
   * Get the serialized Notification object
   * 
   * @return the serialized notification object
   * 
   */
  public function getSerializedDefinition()
  {
    return serialize($this);
  }
  
}
