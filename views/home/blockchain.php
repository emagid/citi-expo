<video type="video/mov" autoplay muted loop src='<?= FRONT_ASSETS ?>img/back.mov' class='background'></video>

<div class='content'>    
<section class='inner_page page'>
    <p class='banner' style='background-color: #00b0b9'>Blockchain</p>
    <p class='close'>BACK</p>
    <img class='img_line' src="<?=FRONT_ASSETS?>img/menu_line.png">

    <div class='links'>
        <p><strong>Blockchain, at its core, features three integrated elements</strong>. Three integrated elements include: </p>
        <ul>
            <li style='color: #00b0b9'><span><strong>Distributed ledger</strong>. A host of digital records that are replicated across multiple entities, or nodes, that can be organized in different ways. If they are organized as blocks, they are called a blockchain</span></li>
            <li style='color: #00b0b9'><span><strong>Validation via cryptography</strong>. Each group of records, or blocks, stores a cryptographic fingerprint called a hash that ensures the authenticity and ownership of the block’s records</span></li>
            <li style='color: #00b0b9'><span><strong>Automated consensus between end-points</strong> to validate information and the certification that only validated transactions are added to the ledger</span></li>
        </ul>
    </div>
</section>
</div>