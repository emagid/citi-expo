<style type="text/css">
	.product_page .content_img img {
		position: relative;
		width: 127%;
	    left: -120px;
	}    

	.product_page .content_img {
		padding-bottom: 100px !important;
	}

	.key {
	    height: 50px;
	    width: auto !important;
	    display: inline-block;
	    margin-right: 55px;
	}

	.key_head {
	    display: inline-block;
	}
</style>

<main>
	<section class="product_page" >

        <!-- Header -->
        <header>
            <a href="/"><img src="<?=FRONT_ASSETS?>img/webair.png"></a>
            <a class='arrow_back' href="/home/products"><img src="<?=FRONT_ASSETS?>img/arrow.png"></a>
        </header>

        <div class='banner'>
        	<div class='banner_overlay'>
        		<img style='height: 111px;' src="<?=FRONT_ASSETS?>img/r_logo.png">
        		<h1>Ransomware Recovery as-a-Service</h1>
        		<div>
        			<!-- <img src="<?=FRONT_ASSETS?>img/about_logo.png"> -->
        		</div>
        	</div>
        </div>

        <div class='content content_img'>
    		<img src="<?=FRONT_ASSETS?>img/r_img.jpg">
    	</div>

        <div class='darken'>
        	<h2>Webair Ransomware Recovery-as-a-Service (RRaaS)</h2>
        	<p>Don't let your business and data be held hostage. Prevent attacks with Webair’s Ransomware Recovery-as-a-Service (RRaaS) solution, which offers fully inclusive, managed and automated Disaster Recovery-as-a-Service (DRaaS) as well as direct integration with threat detection and prevention tools. With data replication that includes pre-planned, automated failover and failback between production and recovery sites, Webair ensures applications are as consumable to users as they were at the production site, allowing for synchronous Recovery Point Objectives (RPOs) and one-hour Recovery Time Objectives (RTOs).</p>
        </div>

        <div class='content'>
        	<img class=' key' src="<?=FRONT_ASSETS?>img/disaster_key.png"><h2 class='key_head'>Key Advantages</h2>
        	<p>Webair’s Cloud-based Disaster Recovery and Off-Site Backup Solutions</p>
        	<h3>Rewind</h3>
        	<p style='margin-top: 0;'>Instantly restore applications with granular change data for up to 30 days</p>
        	<h3>Supported Platforms</h3>
        	<p style='margin-top: 0;'>VMware & Hyper-V, physical servers, IBM i, p, and z series, Azure, AWS, NetApp, Nimble, EMC and all NFS/CIFS</p>
        	<h3>Connectivity</h3>
        	<p style='margin-top: 0;'>Direct network tie-in at recovery sites via any carriers (MPLS, private line)</p>
        	<h3>Automation</h3>
        	<p style='margin-top: 0;'>Network automation via BGP swings, route injection, DNS, and NAC/SDP integration</p>
        	<h3>Multi-location Security</h3>
        	<p style='margin-top: 0;'>Recovery sites in New York, Los Angeles, Montréal, Amsterdam, Hong Kong and Azure</p>
        	<h3>Always-on</h3>
        	<p style='margin-top: 0;'>Production public, private cloud & colocation services at recovery sites</p>
        	<img src="<?=FRONT_ASSETS?>img/r_img2.png">
        </div>
        </section>
</main>

 