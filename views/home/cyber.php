<video type="video/mov" autoplay muted loop src='<?= FRONT_ASSETS ?>img/back.mov' class='background'></video>

<div class='content'>    
<section class='inner_page page'>
    <p class='banner' style='background-color: #c99700'>CyberSecurity</p>
    <p class='close'>BACK</p>
    <img class='img_line' src="<?=FRONT_ASSETS?>img/menu_line.png">

    <div class='links'>
        <p>A recent report estimates that <strong>monetary losses globally from cybercrime are expected to reach a staggering $6 trillion annually by 2021</strong>. The next wave of cybersecurity is leveraging both passive and active biometric information, in an effort to promote a stronger, more secure access and authentication process, while at the same time making it more convenient for clients.</p>
        <ul>
            <li style='color: #c99700'><span><strong>Citi is taking a comprehensive approach to cybersecurity</strong>, leveraging information gathered from the interactions between clients and the bank to develop tailored risk analysis in real time</span></li>
            <li style='color: #c99700'><span><strong>Behavioral biometrics help analyze user interactions</strong> with the banks’ systems in order to help identify a typical activity</span></li>
        </ul>
    </div>
</section>
</div>