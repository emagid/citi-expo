<video type="video/mov" autoplay muted loop src='<?= FRONT_ASSETS ?>img/back.mov' class='background'></video>

<div class='content'>    
<section class='inner_page page booth_page'>
    <p class='banner' style='background-color: #007377'>Vote: favorite booth</p>
    <div class='scroller'>
     <div class='links'>
        <div class="answer">
            <p class='answer_btn'>1</p>
            <p><strong>Citi Velocity</strong> - Your Single Source for Citi’s Data, Content and Analytics</p>
        </div>
        <div class="answer">
            <p class='answer_btn'>2</p>
            <p><strong>CyberSecurity in Action: </strong>- Citi’s Intelligence Led, Fused Cyber Security Approach
 - TTS Cyber Security Advisory & Toolkit </p>
        </div>
        <div class="answer">
            <p class='answer_btn'>3</p>
            <p><strong>Blockchain, Distributed Ledger and Digital Assets </strong>- Current Development in Capital Markets</p>
        </div>
        <div class="answer">
            <p class='answer_btn'>4</p>
            <p><strong>G10 Rates’ XiNG Arena </strong>- Our Platform for Large-Scale Scenario Analysis</p>
        </div>
        <div class="answer">
            <p class='answer_btn'>5</p>
            <p><strong>Equities’ Infinity Cable </strong></p>
        </div>
        <div class="answer">
            <p class='answer_btn'>6</p>
            <p><strong>Markets and Securities Services Contextual Recommendation Launchpad </strong></p>
        </div>
        <div class="answer">
            <p class='answer_btn'>7</p>
            <p><strong>Spread Products’ Advanced Analytics for Bond Trading </strong></p>
        </div>
        <div class="answer">
            <p class='answer_btn'>8</p>
            <p><strong>FX & Commodities’ Citi Velocity </strong>- Our Innovative, Award Winning Trading Platform</p>
        </div>
        <div class="answer">
            <p class='answer_btn'>9</p>
            <p><strong>Citi Futures Online </strong></p>
        </div>
        <div class="answer">
            <p class='answer_btn'>10</p>
            <p><strong>Customer Relationship Management </strong>(CRM)</p>
        </div>
        <div class="answer">
            <p class='answer_btn'>11</p>
            <p><strong>Machine Learning and Natural Language Processing </strong>for Automated Onboarding</p>
        </div>
        <div class="answer">
            <p class='answer_btn'>12</p>
            <p><strong>Citi Private Bank Digital Capabilities for Global Citizens </strong></p>
        </div>
        <div class="answer">
            <p class='answer_btn'>13</p>
            <p><strong>Service Innovation </strong>- Service Lookup Tool and Project Jasper</p>
        </div>
        <div class="answer">
            <p class='answer_btn'>14</p>
            <p><strong>Ecosystems of Spread Products </strong>– Origination, Syndicate and Trading</p>
        </div>
        <div class="answer">
            <p class='answer_btn'>15</p>
            <p><strong>Data Access, </strong>Discovery and Analytics</p>
        </div>
        <div class="answer">
            <p class='answer_btn'>16</p>
            <p><strong>Client Communication and Exception Management </strong></p>
        </div>
        <div class="answer">
            <p class='answer_btn'>17</p>
            <p><strong>Future of Work: </strong>The Profile of Success for Tomorrow</p>
        </div>
        <div class="answer">
            <p class='answer_btn'>18</p>
            <p><strong>Degreed: </strong> Learn, Share, Collaborate</p>
        </div>
        <div class="answer">
            <p class='answer_btn'>19</p>
            <p><strong>Cornell Tech: </strong>A Unique Partnership with Citi</p>
        </div>
        <div class="answer">
            <p class='answer_btn'>20</p>
            <p><strong>TTS: </strong>Digitizing Sales and Solutioning</p>
        </div>
        <div class="answer">
            <p class='answer_btn'>21</p>
            <p><strong>TTS: </strong>Transforming the Implementation and Connectivity Experience</p>
        </div>
        <div class="answer">
            <p class='answer_btn'>22</p>
            <p><strong>TTS: </strong>Accelerating the Onboarding Experience</p>
        </div>
        <div class="answer">
            <p class='answer_btn'>23</p>
            <p><strong>TTS: </strong>CitiDirect BE® Digital Bar</p>
        </div>
        <div class="answer">
            <p class='answer_btn'>24</p>
            <p><strong>TTS: </strong>Digitizing Trade</p>
        </div>
        <div class="answer">
            <p class='answer_btn'>25</p>
            <p><strong>TTS: </strong>Treasury Disruption Powered by Data and AI</p>
        </div>
        <div class="answer">
            <p class='answer_btn'>26</p>
            <p><strong>TTS: </strong>Powering Global Liquidity Management</p>
        </div>
        <div class="answer">
            <p class='answer_btn'>27</p>
            <p><strong>TTS: </strong> Innovating Payments & Receivables for the Future</p>
        </div>
        <div class="answer">
            <p class='answer_btn'>28</p>
            <p><strong>TTS: </strong>Next Generation Payments and Receivables Infrastructure and Platforms</p>
        </div>
        <div class="answer">
            <p class='answer_btn'>29</p>
            <p><strong>TTS: </strong>Driving Digital Innovation and Simplicity in Global Card Solutions</p>
        </div>

        <div class="answer">
            <p class='answer_btn'>30</p>
            <p><strong>TTS: </strong>Global Innovation Lab - Igniting Innovation Ecosystem</p>
        </div>
        <div class="answer">
            <p class='answer_btn'>31</p>
            <p><strong>ICG Credit Redesign and Lending Digitization: </strong>- Re-Shaping Our Credit Process and Technology Platforms to Drive a Step-Change Improvement in Client Responsiveness</p>
        </div>
        <div class="answer">
            <p class='answer_btn'>32</p>
            <p><strong>Citi Ventures</strong></p>
        </div>
    </div>

        <p class='complete'>Thank You!</p>
    </div>

        <form id="vote">
            <input id="vote_ans" type="hidden" name="vote_answer" value="">
            <input id="sumit_btn" class="button" type="submit" value="VOTE">
        </form>
        <i class="fas fa-angle-down"></i>

    <div class='popup'>
        <div class='off_click'></div>
        <p class='close'>X</p>
        <div class=holder>
            <p></p>
            <button id='submit_btn'>SUBMIT VOTE</button>
        </div>
    </div>






<!--     <div class=''>
        
    </div> -->

</section>
    
<script type="text/javascript">
    $(document).ready(function(){
        $(document).on("click",".links .answer", function(e){
            $('.links .answer p:nth-child(2)').removeClass('chosen');
            $('.links .answer p:nth-child(2)').css('background', 'transparent');
            $('.answer').css('pointer-events', 'none');

            var ans = $(this).text();
            var color = $(this).children('p:nth-child(1)').css('background');
            var txt = $(this).children('p:nth-child(2)');

            $(txt).addClass('chosen');
            $(txt).css('background', color);
            $('#vote_ans').val(ans);

            $('.popup').fadeIn(500);
            $('.popup').css('display', 'flex');
            $('.popup .holder').css('background', color);
            $('.popup .holder p').html(txt.clone());
            setTimeout(function(){
                $('.answer').css('pointer-events', 'all');
            }, 500);
        });

    $(document).on('click', '.fa-angle-down', function(){
        $('.scroller').animate({
            scrollTop : 2100
        }, 1000);
    });

    $(document).on('click', '.off_click, .close', function(){
        $('.popup').fadeOut(500);
    });

        // $('.booths .booth .booth_option').click(function(e){
        //     console.log($(this).text());
        //     var ans = $(this).text();
        //     $('#vote_ans').val(ans);
        // })


        $(document).on('click', '#submit_btn', function(){
            $('#vote').submit();
        });


       $(document).on('submit', "#vote", function(e){
            e.preventDefault();
            $.post("/home/booth", $(this).serialize(), function(response){
                if(response.status){
                    $('.holder').slideUp(500);
                    setTimeout(function(){
                        $('.holder').slideDown(500);
                        $('.holder p').html('Thank you!').css('text-align', 'center');
                        $('.holder button').hide();
                    }, 500);

                    setTimeout(function(){
                        window.location.href = '/';
                    }, 3000);
                }
            });
        });
    });
</script>





</div>
