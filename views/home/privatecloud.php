<style type="text/css">
	.product_page .content img {
		position: relative;
	    width: 112%;
	    left: -57px;
	}    
</style>

<main>
	<section class="product_page" >

        <!-- Header -->
        <header>
            <a href="/"><img src="<?=FRONT_ASSETS?>img/webair.png"></a>
            <a class='arrow_back' href="/home/products"><img src="<?=FRONT_ASSETS?>img/arrow.png"></a>
        </header>

        <div class='banner'>
        	<div class='banner_overlay'>
        		<img style='height: 111px;' src="<?=FRONT_ASSETS?>img/pc_logo.png">
        		<h1>Enterprise Virtual Private Cloud (E-VPC)</h1>
        		<div>
        			<!-- <img src="<?=FRONT_ASSETS?>img/about_logo.png"> -->
        		</div>
        	</div>
        </div>

    	<div class='content content_img'>
    		<img src="<?=FRONT_ASSETS?>img/pc_img.png">
    	</div>

        <div class='content'>
        	<p>Webair’s VMware-based Enterprise Virtual Private Cloud (E-VPC) is an Infrastructure-as-a-Service (IaaS) platform that enables businesses to develop, deploy, manage and scale solutions with ease without having to purchase and manage hardware or make up-front capital investment. Webair takes ownership and management of the infrastructure, security, scaling, backups and disaster recovery. Webair also offers management of server operating systems and popular application stacks.</p>
        	<p>Webair’s E-VPC is purpose-built to run the most critical and demanding workloads on secure, reliable, high performance and wholly-owned and operated enterprise cloud infrastructure – backed by a 99.9% uptime SLA guarantee and 24x7x365 support. The solution is also HIPAA, CJIS, FISMA and PCI-compliant. Furthermore, Webair signs HIPAA Business Associate Agreements (BAAs) with customers.</p>
        </div>
        </section>
</main>

 