<video type="video/mov" autoplay muted loop src='<?= FRONT_ASSETS ?>img/back.mov' class='background'></video>

<div class='content'>    
<section class='inner_page page'>
    <p class='banner' style='background-color: #c6007e'>Survey</p>
    <p class='close'>BACK</p>
    <img class='img_line' src="<?=FRONT_ASSETS?>img/menu_line.png">

    <div class='links'>
        <p class='survey_count'>1 / <?=count($model->questions)?></p>
        <div class='questions'>
	        <?php foreach ($model->questions as $i => $question) {?>
	        	<div class='survey'>
		            <p class='question'><?=$question->text?><br><br><strong>Click all that apply</strong></p>
			            <? $choices = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ' ?>
			            <?foreach ($question->answers as $a => $answer) {?>
				            <div class="answer">
				                <p class='answer_btn'><?=substr($choices,$a,1)?></p>
				                <p><?=$answer->text?></p>
				                <!-- <? if ($answer->featured_image != null && $answer->featured_image != '') {?>
				                    <img src="<?=UPLOAD_URL . 'answers/' . $answer->featured_image ?>" width="100"/>
				                <? } ?> -->
				            </div>
			            <? } ?>
			            <button class="button">NEXT</button>
	        	</div>
		    <? } ?>
        </div>
        <p class='complete'>Thank You!</p>
    </div>
</section>



	<script>
    $(document).ready(function () {
        $(document).on('click', '.answer', function(){
        	if ( $(this).children('p:nth-child(2)').hasClass('chosen') ) {
	        	$(this).children('p:nth-child(2)').removeClass('chosen').css('background', 'transparent');
        	}else {
	        	var color = $(this).children('.answer_btn').css('background');
	        	$(this).children('p:nth-child(2)').addClass('chosen').css('background', color);
        	}
        });


      // Next questions
      var timer;
	  var answers = [];
      var question_num = 1;

     $('.survey .button').click(function(){
     	var survey = $(this).parents('.survey');
     	var self = this;

	      if ( $(survey).children('.answer').children('p:nth-child(2)').hasClass('chosen') ) {
		      var total_questions = $('.survey_count').html().substring($('.survey_count').html().length - 2);
		      $(self).css('pointer-events', 'none');
		     	$(survey).fadeOut(500);
		     	timer = setTimeout(function(){
		     		$(survey).next('.survey').fadeIn(500);
		     	}, 500);

		       var answer = [];
		       $(survey).children('.answer').children('p.chosen').each(function(){
		       		answer.push($(this).html());
		       });

		       answers.push(answer);
		       if ( parseInt($('.survey_count').html()) ==  parseInt(total_questions)) {
		       		// thank you, reload to home, send info
		       		timer = setTimeout(function(){
			       		$('.complete').fadeIn();
			     	}, 500);

		       		timer = setTimeout(function(){
			       		window.location = '/';
			     	}, 5000);
		       }else {
		       	timer = setTimeout(function(){
			       $('.survey_count').html((question_num += 1).toString() + "  / " + total_questions );
		       	}, 1000);
		       }
	      }
     });
        
    });
</script>



</div>


