<style type="text/css">
	.product_page .content_img img {
		width: 100%;
	}    

	.product_page .content_img {
		padding: 0 !important;
	}

	.product_page .content {
		padding-top: 0;
	}
</style>

<main>
	<section class="product_page" >

        <!-- Header -->
        <header>
            <a href="/"><img src="<?=FRONT_ASSETS?>img/webair.png"></a>
            <a class='arrow_back' href="/home/products"><img src="<?=FRONT_ASSETS?>img/arrow.png"></a>
        </header>

        <div class='banner'>
        	<div class='banner_overlay'>
        		<img style='height: 111px;' src="<?=FRONT_ASSETS?>img/pc_logo.png">
        		<h1>Enterprise Virtual Private Cloud (E-VPC)</h1>
        		<div>
        			<!-- <img src="<?=FRONT_ASSETS?>img/about_logo.png"> -->
        		</div>
        	</div>
        </div>

    	<div class='content content_img'>
    		<img src="<?=FRONT_ASSETS?>img/h_img.jpg">
    	</div>

        <div class='content'>
        	<p>Enterprises’ critical infrastructure is increasingly becoming multi-platform and hybrid, driven in large part by maturing Software-as-a-Service (SaaS) and cloud services, and the flexibility they allow companies when it comes to selecting the ideal platform for each workloads or application, whether that be on-premise, in an edge data center, public cloud, or with a SaaS provider. Consisting of multiple platforms and bound together by an intricate network, these hybrid environments can also be highly complex to manage. Balancing the need to sustain effective operations while also empowering IT to improve its service delivery agility is why enterprises turn to Webair to be their single, trusted advisor for managing connectivity, security and performance across their hybrid environments.</p>
        </div>
        </section>
</main>

 