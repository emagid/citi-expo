<video type="video/mov" autoplay muted loop src='<?= FRONT_ASSETS ?>img/back.mov' class='background'></video>

<div class='content'>    
<section class='inner_page page'>
    <p class='banner' style='background-color: #c4d600'>Digital Onboarding</p>
    <p class='close'>BACK</p>
    <img class='img_line' src="<?=FRONT_ASSETS?>img/menu_line.png">

    <div class='links'>
        <p><strong>Citi is re-imagining the client onboarding experience to make it digital, simple and fast</strong>. Our new digital onboarding allows clients to review and complete onboarding requirements via an easy-to-use, guided interface.</p>
        <ul>
            <li style='color: #c4d600'><span>As bank agnostic solutions develop, <strong>Citi is actively working to partner with these new utilities as a new source of onboarding information</strong></span></li>
            <li style='color: #c4d600'><span><strong>The new CitiDirect BE &#174; Digital Onboarding experience</strong> will allow you to complete the Know Your Customer (KYC) and Account Opening requirements 100% digitally, as well as track the status of your account opening</span></li>
        </ul>
    </div>
</section>
</div>