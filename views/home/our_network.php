<style type="text/css">
	.box > div {
		background-position: center center;
		background-size: cover;
		height: 300px;
		position: relative;
	}

	.box > div div {
		background-color: #31a1dca3;
		position: absolute;
		height: 100%;
		width: 100%;
		display: flex;
		justify-content: center;
		align-items: center;
	}

	.box > div h2 {
		color: white;
	}

	.box p {
		margin-bottom: 75px;
	    font-size: 24px;
	}
</style>

<main>
	<section class="product_page" >

        <!-- Header -->
        <header>
            <a href="/"><img src="<?=FRONT_ASSETS?>img/webair.png"></a>
            <a class='arrow_back' href="/home/products"><img src="<?=FRONT_ASSETS?>img/arrow.png"></a>
        </header>

        <div class='banner'>
        	<div class='banner_overlay'>
        		<img style='height: 111px;' src="<?=FRONT_ASSETS?>img/pc_logo.png">
        		<h1>Our Network</h1>
        	</div>
        </div>

        <div class='content'>
        	<div class='boxes'>
        		<div class='box'>
        			<div style="background-image: url('<?=FRONT_ASSETS?>img/n_img1.jpg')">
        				<div>
		        			<h2>New York</h2>
        				</div>
        			</div>
        			<p>Long Island’s most secure, fully redundant data center, offering hybrid capabilities that enable scaling from colo to cloud – all in the same facility.</p>
        		</div>
        		<div class='box'>
        			<div style="background-image: url('<?=FRONT_ASSETS?>img/n_img2.jpg')">
	        			<div>
		        			<h2>Los Angeles</h2>
        				</div>
        			</div>
        			<p>Best-in-class Colo, Cloud and Managed Service solutions, as well as unparalleled connectivity in the telco epicenter of the Pacific Rim.</p>
        		</div>
        		<div class='box'>
        			<div style="background-image: url('<?=FRONT_ASSETS?>img/n_img3.jpg')">
	        			<div>
		        			<h2>Amsterdam</h2>
        				</div>
        			</div>
        			<p>Superior connectivity and power in one of the Netherlands’ state-of-the-art data centers located in one of Europe's emerging technology hubs.</p>
        		</div>
        		<div class='box'>
        			<div style="background-image: url('<?=FRONT_ASSETS?>img/n_img4.jpg')">
	        			<div>
		        			<h2>Montreal</h2>
        				</div>
        			</div>
        			<p>Montreal’s primary carrier hotel, offering direct access to 70+ providers, optimal power and redundancy, and long-haul fiber routes to Europe.</p>
        		</div>
        	</div>
        </div>
    </section>
</main>

 