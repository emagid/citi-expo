<video type="video/mov" autoplay muted loop src='<?= FRONT_ASSETS ?>img/back.mov' class='background'></video>

<div class='content'>    
<section class='inner_page page'>
    <p class='banner' style='background-color: #ffcd00'>Client Experience</p>
    <p class='close'>BACK</p>
    <img class='img_line' src="<?=FRONT_ASSETS?>img/menu_line.png">

    <div class='links'>
        <p><strong>Putting the client at the center of everything we do.</strong></p>
        <ul>
            <li style='color: #ffcd00'><span><strong>Citi strives to provides clients with a seamless end to end experience across all aspects of the client journey</strong>, moving to an increasingly automated and fully digital approach</span></li>
            <li style='color: #ffcd00'><span><strong>Ideating and co-creating with our clients</strong> by observing behavior, analyzing challenges and discussing everyday</span></li>
        </ul>
    </div>
</section>
</div>