<?php
 if(count($model->votes)>0): ?>
  <div class="box box-table">
    <table class="table">
      <thead>
        <tr>
          <th width="15%">No.</th>
          <th width="50%">Favorite Booth Vote</th>
          <!-- <th width="15%" class="text-center">Edit</th>
          <th width="15%" class="text-center">Delete</th>	 -->
        </tr>
      </thead>
      <tbody>
       <?php foreach($model->votes as $key=>$obj){ 
          $question = \Model\Question::getItem($obj->question_num);?>
        <tr>
            <td><a><?php echo $key+1; ?></a></td>
            <td><a><?php echo $obj->fav_booth; ?></a></td>
       </tr>
       <?php } ?>
   </tbody>
 </table>
 <div class="box-footer clearfix">
  <div class='paginationContent'></div>
</div>
</div>
<?php endif; ?>

<?php echo footer(); ?>
<script type="text/javascript">
	var site_url = '<?= ADMIN_URL.'votes';?>';
    var total_pages = <?= $model->pagination->total_pages;?>;
    var page = <?= $model->pagination->current_page_index;?>;
</script>