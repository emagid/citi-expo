<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data" >
    <input type="hidden" name="id" value="<?php echo $model->ring_material->id; ?>" />
    <div class="row">
        <div class="col-md-24">
            <div class="box">
                <h4>General</h4>
                <div class="form-group">
                    <label>Name</label>
                    <input name="name" type="text" value="<?=$model->ring_material->name?>" />
                </div>
                <div class="form-group">
                    <label>Price</label>
                    <?php echo $model->form->editorFor("price"); ?>
                </div>
                <div class="form-group">
                    <label>Minimum Size</label>
                    <?php echo $model->form->editorFor("min_size"); ?>
                </div>
                <div class="form-group">
                    <label>Maximum Size</label>
                    <?php echo $model->form->editorFor("max_size"); ?>
                </div>
                <div class="form-group">
                    <label>Size Step</label>
                    <?php echo $model->form->editorFor("size_step"); ?>
                </div>
                <div class="form-group">
                    <label>Size Step Price</label>
                    <?php echo $model->form->editorFor("size_step_price"); ?>
                </div>
            </div>
        </div>
    </div>
    <button type="submit" class="btn btn-save">Save</button>
</form>
<?php footer(); ?>
