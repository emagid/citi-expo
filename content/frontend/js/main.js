$(document).ready(function(){
    // no rights clicks
    document.addEventListener('contextmenu', event => event.preventDefault());

    // $('header').css('margin-top', '1710px');

    // options animation 



    $('.menu').click(function(){
        $('.content').fadeOut(500);
        $(this).addClass('open');
        $('.jQKeyboardContainer').slideUp(500);
        setTimeout(function(){
            $('.main_menu').fadeIn();
        }, 550);
    });

    $('.close').click(function(){
        $('.main_menu').fadeOut();
        setTimeout(function(){
            $('header .menu').removeClass('open');
            $('.content').fadeIn(500);
        }, 550);
    });


    var vid = $('.background');
    vid[0].playbackRate = 0.5;

    if ( $('.home').hasClass('photobooth') ) {
        $('#video').fadeIn();
        $('#video').fadeIn();
        $('.pic_text').fadeIn(2000);
        $('#snap_photo').fadeIn(2000);
        $('#snap_gif').fadeIn(2000);
    };

    $('.prize').click(function(){
        $('.white').fadeIn(500);
        setTimeout(function(){
            window.location.href = '/trivia';
        }, 700);
    });


    // / Choice click
    var timer;
    $(".click_action").on({
         'click': function clickAction() {
             var self = this;
                $(self).css('transform', 'scale(.8)');
              timer = setTimeout(function () {
                  $(self).css('transform', 'scale(1)');
              }, 100);
         }
    });


    // Choice click delay
    $('.option').on('click', function(e){
        var link = $(this).attr("href");
        e.preventDefault()
            $('.option').css('opacity', '0'); 
            $(this).css('opacity', '1'); 
            $(this).css('transform', 'scale(1.4)'); 
            $('.himms').fadeOut();
        setTimeout(function(){
            if ( $(this).not('#choose_picture') ) {
                window.location.href = link;
            }
        }, 1000);
    });

    // $('a').on('click', function(e){
    //     var link = $(this).attr("href");
    //     e.preventDefault()
    //     $('.white').fadeIn(500);
    //     setTimeout(function(){
    //         window.location.href = link;
    //     }, 700);
    // });


    // event pics Button slide
    $('.event_pics .overlay').on('click', function(){
        $('.photos').css('opacity', '0');


        // $(this).css('background-color', '#348eca');
        $(this).children('h2').fadeOut(1500);

        setTimeout(function(){
            $('.event_pics').css('height', '100%');
            $('.event_pics').addClass('active');
            $('.overlay').css('background-color', 'transparent');

            $('.photos').hide();
            $('.inner_content').slideDown(200);
            $('.display_images').slideDown();
            $('.choice').css('opacity', '1');

        }, 1000);

        setTimeout(function(){
            $('#home_click').fadeIn();
            $('.event_pics').css('min-height', '1920px');
            $('.event_pics').css('touch-events', 'none');
        }, 1500);
    });


    // Home click
    $('#home_click').on('click', function(){
        $('#jQKeyboardContainer').remove()

            $('.photos').removeClass('photo_booth_active')
            $(this).fadeOut();
            $('.inner_content').fadeOut();
            $('#donate_form')[0].reset();
    });


    $('.fa-close').click(function(){
        $('.share_overlay').slideUp();
        $('.jQKeyboardContainer').slideUp();
        $(this).fadeOut();
    })




    // Take Photo click
     $('#take_pic').on('click', function(e){
        // e.preventDefault()
        // $('.option').css('opacity', '0'); 
        // $(this).css('opacity', '1'); 
        // $(this).css('transform', 'scale(1.4)'); 
        // $('.himms').fadeOut();
        // $(this).delay(1000).fadeOut();

        $(this).addClass('photo_booth_active')
        $('.holiday').fadeOut();
        $('#slider').fadeOut();
        $('.donate').css('opacity', '0');
        $('.about').css('opacity', '0');
        $('.event_pics').css('opacity', '0');
        $('#photos').css('margin-bottom', '0');
        setTimeout(function(){
            $('.donate').hide();
            $('.about').hide();
            $('.event_pics').hide();

            $('.photos .button').fadeOut();
            $('.photos').css('height', '1920px');
            $('#video').delay(1000).fadeIn();
            $('#snap_photo').delay(1500).fadeIn();
            $('.pic_text').delay(1500).fadeIn();
            $('#home_click').delay(1500).fadeIn();
        }, 1000);
    });





     // Checkmark
    var count = (function (num) {
        var counter = 0;
        return function (num) {return counter += num;}
    })();
    

     $(document).on('click', '.gif', function(){
        var num = count(1);
        if ( !$(this).hasClass('checked') && num < 5 ) {            
            if ( num <= 4  ) {
                $(this).css('outline', 'rgb(69, 82, 103) solid 5px')
                $(this).addClass('checked');
                if ( num  === 4 ) {
                    $('.gif_info').fadeOut();
                    $('.next').slideDown();
                    $('html, body').animate({
                        scrollTop: $(".home").offset().top
                    }, 500);
                }
            }

        }else if ( $(this).hasClass('checked') ) {
            num = count(-2);
            $(this).removeClass('checked');
            $(this).css('outline', '0px')
            if ( num  === 4 ) {
                    $('.next').slideDown();
                    $('.gif_info').fadeOut();
                    $('html, body').animate({
                        scrollTop: $(".home").offset().top
                    }, 500);

            }else {
                $('.next').slideUp();
                $('.gif_info').fadeIn();

            }

        }else {
            num = count(-1);
        }
     });




     // Gif Next click
     function picAction( pic ) {
      $(pic).fadeIn('fast');
      $(pic).delay(1300).fadeOut(400);
    }

     function gif( pics ) {
      var offset = 0

      for ( i=0; i<2; i++ ) {
        pics.each(function(){
          var timer
          var self = this;
          timer = setTimeout(function(){
              picAction(self)
          }, 0 + offset);

          offset += 1500;
        });
      }

      setTimeout(function(){
            var gif_pics = $('.gif_show').children('.checked');
            gif( gif_pics )
        }, 12000);
    }

     $(document).on('click', '.next', function(){
        $('#pictures').fadeOut();
        $('.gif_show').fadeIn();
        $('.gif_show').addClass('flex');

        // =========  NEED TO TURN IMAGES INTO GIF TO SHOW HERE  ================
        $('#submit_form').find('.image_encoded').remove();
        $('.canvas_holder.checked img').each(function () {
            $('#submit_form').append($('<input type="hidden" name="images[]" value="'+$(this).attr('src')+'">'));
        });
        let deferreds = [];
        let imgs = [];
        $("#submit_form input[name='images[]']").each(function(i,el){
            deferreds.push(
                $.post('/contact/make_frame/', {
                    image: el.value
                },function(data) {
                    var newref = window.location.href.substring(0, 27);
                    imgs.push(newref+'content/uploads/Snapshots/'+data.image.image);
                    $('input[name="images[]"]')[i].value = data.image.id;
                }));
        });


        //Show loading image
        $.when(...deferreds).then( function() {
             gifshot.createGIF({
                 'images': imgs,
                 'frameDuration':4,
                 'gifWidth': 900,
                 'gifHeight': 1200
             },function(obj) {
                 if(!obj.error) {
                     var image = obj.image;
                     console.log(image)
                     $('#loader').fadeOut();
                     $('.gif_show').append("<img src=' " + image + "'>")
                     $('#submit_form').append($('<input type="hidden" name="gif" value="'+image+'">'));
                     $.post('/contact/save_img/',$('#submit_form').serialize(),function(data){
                         $('#submit_form input[name=gif]').val(data.gif.id);
                     });
                 }
             });
             // imgs = imgs.join();

             // imgs = 'http://cdn4.gurl.com/wp-content/uploads/2014/03/woman-pointing-at-self.jpg,https://thumb1.shutterstock.com/display_pic_with_logo/1729711/280157228/stock-photo-model-isolated-pointing-to-herself-280157228.jpg,http://tse4.mm.bing.net/th?id=OIP.hz6jv2fJu8OPK4x6s6ybbAEsDI&w=300&h=200&pid=1.1,https://idc-static.s3.amazonaws.com/seo/pointing%20at%20myself.jpg';

             // $.ajax('https://udayogra-images-to-gif-converter-v1.p.mashape.com/am',
             //     {
             //         type: 'GET',
             //         data: {
             //             delay: 500,
             //             imageurls: imgs
             //         },
             //         datatype: 'json',
             //         beforeSend: function(xhr) {
             //             xhr.setRequestHeader("X-Mashape-Authorization", "TbbShja6Qtmsh8WPaTF6sb6GCXmbp18UoSRjsnja4sFYJCwcjc");
             //         },
             //         success: function(data) {
             //             //remove loading image
             //             $('#loader').fadeOut();
             //             var giflink = data.giflink;  //gifurl
             //             $('.gif_show').append("<img src=' " + giflink + "'>")
             //             $('#submit_form').append($('<input type="hidden" name="gif" value="'+giflink+'">'));
             //             $.post('/contact/save_img/',$('#submit_form').serialize(),function(data){
             //                 $('#submit_form input[name=gif]').val(data.gif.id);
             //             });
             //         }
             //     });
         });


        $('.submit').delay(1000).fadeIn();
        // $('.like').hide();

        // var pics = $('#pictures').children('.checked');

        // for ( i=0; i<4; i++ ) {
        //     $('.gif_show').append(pics[i]);
        // }

        // var gif_pics = $('.gif_show').children('.checked');
        //         // console.log(gif_pics)

        // // setting up order for gif loop
        // $('.gif_show').children('.checked').hide();
        // // $($('.gif_show').children('.checked')[0]).show();

        // for ( i=0; i<4; i++ ) {
        //     $('.gif_show').removeClass('checked');
        //     $($($('.gif_show').children()[i]).children()[0]).hide();
        //     // $($('.checked').children()[0]).hide();        
        // }

        // gif( gif_pics )
     });
    

     // Share click
     $(document).on('click', '.submit', function(){
        $('.share_overlay').fadeIn();
        $('#pictures').fadeOut();
     });

      $('#share_btn').click(function(){
        $('.fa-close').fadeOut();
        $('.jQKeyboardContainer').fadeOut();
     })

     function saveDrawing() {
            
            var draw_canvas = $('#draw');
            $($(draw_canvas[0]).parents('#pictures')[0]).append(convertCanvasToImage(draw_canvas[0]))

            $('.drawing').hide();
        }
     


     // Form submit
     $('#submit_form').on('submit', function(e){
        e.preventDefault();
        $.post('/home/selfie', $(this).serialize(), function (response) {
            if(response.status){
                $('#submit_form')[0].reset();
                $('#share_alert').slideDown();
                $('#share_alert').css('display', 'flex');

                setTimeout(function(){
                    $('#share_alert').slideUp();
                    window.location.href = '/';
                }, 3000);
            }
        });
     });


     // Click of done sharing
     $('#done').on('click', function(e){
        $('.share_overlay').slideUp();
        $('#jQKeyboardContainer').remove()
        // remove all images
        $('#pictures').children('.canvas_holder');
     });


     // click of showing images
     $('.event_images').on('click', function(){
        $('.choice').fadeOut();
        $('#event_gifs').fadeIn();
        $('.display_images').slideDown();
     });


      // click of showing gifs
     $('.event_gifs').on('click', function(){
        $('.choice').fadeOut();
        $('#event_images').fadeIn();
        $('.display_gifs').slideDown();
     });


      // click of showing gifs when images are shown
     $('#event_gifs').on('click', function(){
        $('#event_images').fadeIn();
        $('#event_gifs').fadeOut();
        $('.display_images').slideUp();
        setTimeout(function(){
            $('.display_gifs').slideDown();
        }, 1000)
     });

     // click of showing images when gifs are shown
     $('#event_images').on('click', function(){
        $('#event_gifs').fadeIn();
        $('#event_images').fadeOut();
        $('.display_gifs').slideUp();
        setTimeout(function(){
            $('.display_images').slideDown();
        }, 1000)
     });


     $('.print').click(function(){
          saveDrawing();  
          var source = $($('#pictures img:last-child')[1]).attr('src');
          VoucherPrint(source);
     });


     function convertCanvasToImage(canvas) {
        var image = new Image();
        image.src = canvas.toDataURL("image/png");
        console.log(image)
        return image;
    }


     function VoucherSourcetoPrint(source) {
      return "<html><head><title>Powered by Popshap.com</title><style>/* style sheet for 'A4' printing */"+
        "@page {" +
         "size: A4;" +
         "margin: 0%;"+
         "}"+ 
         "</style><script>function step1(){\n" +
          "setTimeout('step2()', 10);}\n" +
          "function step2(){window.print();window.close()}\n" +
          "</script></head><body onload='step1()'>\n" +
          "<img src='" + source + "' /></body></html>";
    }
    function VoucherPrint(source) {
      Pagelink = "about:blank";
      var pwa = window.open(Pagelink, "_new");
      pwa.document.open();
      pwa.document.write(VoucherSourcetoPrint(source));
      pwa.document.close();
    }


     // initial timeout redirect homepage
        var initial = null;

        function invoke() {
            initial = window.setTimeout(
                function() {
                    $('.white').fadeIn();
                    setTimeout(function(){
                        window.location.href = '/';
                    }, 700)
                }, 60000);
        }


        invoke();

        $('body').on('click mousemove', function(){
            window.clearTimeout(initial);
            invoke();
            // invoke2();
        })



        var keyboard;
            $(function(){
                keyboard = {
                    'layout': [
                        // alphanumeric keyboard type
                        // text displayed on keyboard button, keyboard value, keycode, column span, new row
                        [
                            [
                                ['@', '@', 192, 0, true], ['1', '1', 49, 0, false], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                                ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['-', '-', 189, 0, false], ['=', '=', 187, 0, false],
                                ['q', 'q', 81, 0, true], ['w', 'w', 87, 0, false], ['e', 'e', 69, 0, false], ['r', 'r', 82, 0, false], ['t', 't', 84, 0, false], ['y', 'y', 89, 0, false], ['u', 'u', 85, 0, false], 
                                ['i', 'i', 73, 0, false], ['o', 'o', 79, 0, false], ['p', 'p', 80, 0, false], ['[', '[', 219, 0, false], [']', ']', 221, 0, false], ['&#92;', '\\', 220, 0, false],
                                ['a', 'a', 65, 0, true], ['s', 's', 83, 0, false], ['d', 'd', 68, 0, false], ['f', 'f', 70, 0, false], ['g', 'g', 71, 0, false], ['h', 'h', 72, 0, false], ['j', 'j', 74, 0, false], 
                                ['k', 'k', 75, 0, false], ['l', 'l', 76, 0, false], [';', ';', 186, 0, false], ['&#39;', '\'', 222, 0, false], ['Enter', '13', 13, 3, false],
                                ['Shift', '16', 16, 2, true], ['z', 'z', 90, 0, false], ['x', 'x', 88, 0, false], ['c', 'c', 67, 0, false], ['v', 'v', 86, 0, false], ['b', 'b', 66, 0, false], ['n', 'n', 78, 0, false], 
                                ['m', 'm', 77, 0, false], [',', ',', 188, 0, false], ['.', '.', 190, 0, false], ['/', '/', 191, 0, false], ['Shift', '16', 16, 2, false],
                                ['Bksp', '8', 8, 3, true], ['Space', '32', 32, 12, false], ['Clear', '46', 46, 3, false], ['Cancel', '27', 27, 3, false]
                            ]
                        ]
                    ]
                }
                $('input.jQKeyboard').initKeypad({'keyboardLayout': keyboard});
                $('textarea.jQKeyboard').initKeypad({'keyboardLayout': keyboard});


                var board = {
                    'layout': [
                        // alphanumeric keyboard type
                        // text displayed on keyboard button, keyboard value, keycode, column span, new row
                        [
                                ['@', '@', 192, 0, true], ['1', '1', 49, 0, false], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                                ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['Bksp', '8', 8, 3, true]
                        ]
                    ]
                }
                $('input.donate_key').initKeypad({'donateKeyboardLayout': board});
            });

      (function($){

    var keyboardLayout = {
        'layout': [
            // alphanumeric keyboard type
            // text displayed on keyboard button, keyboard value, keycode, column span, new row
            [
                [
                    ['@', '@', 192, 0, true], ['1', '1', 49, 0, false], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                    ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['-', '-', 189, 0, false], ['=', '=', 187, 0, false],
                    ['q', 'q', 81, 0, true], ['w', 'w', 87, 0, false], ['e', 'e', 69, 0, false], ['r', 'r', 82, 0, false], ['t', 't', 84, 0, false], ['y', 'y', 89, 0, false], ['u', 'u', 85, 0, false], 
                    ['i', 'i', 73, 0, false], ['o', 'o', 79, 0, false], ['p', 'p', 80, 0, false], ['[', '[', 219, 0, false], [']', ']', 221, 0, false], ['&#92;', '\\', 220, 0, false],
                    ['a', 'a', 65, 0, true], ['s', 's', 83, 0, false], ['d', 'd', 68, 0, false], ['f', 'f', 70, 0, false], ['g', 'g', 71, 0, false], ['h', 'h', 72, 0, false], ['j', 'j', 74, 0, false], 
                    ['k', 'k', 75, 0, false], ['l', 'l', 76, 0, false], [';', ';', 186, 0, false], ['&#39;', '\'', 222, 0, false], ['Enter', '13', 13, 3, false],
                    ['Shift', '16', 16, 2, true], ['z', 'z', 90, 0, false], ['x', 'x', 88, 0, false], ['c', 'c', 67, 0, false], ['v', 'v', 86, 0, false], ['b', 'b', 66, 0, false], ['n', 'n', 78, 0, false], 
                    ['m', 'm', 77, 0, false], [',', ',', 188, 0, false], ['.', '.', 190, 0, false], ['/', '/', 191, 0, false], ['Shift', '16', 16, 2, false],
                    ['Bksp', '8', 8, 3, true], ['Space', '32', 32, 12, false], ['Clear', '46', 46, 3, false], ['Cancel', '27', 27, 3, false]
                ],
                [
                    ['~', '~', 192, 0, true], ['!', '!', 49, 0, false], ['@', '@', 50, 0, false], ['#', '#', 51, 0, false], ['$', '$', 52, 0, false], ['%', '%', 53, 0, false], ['^', '^', 54, 0, false], 
                    ['&', '&', 55, 0, false], ['*', '*', 56, 0, false], ['(', '(', 57, 0, false], [')', ')', 48, 0, false], ['_', '_', 189, 0, false], ['+', '+', 187, 0, false],
                    ['Q', 'Q', 81, 0, true], ['W', 'W', 87, 0, false], ['E', 'E', 69, 0, false], ['R', 'R', 82, 0, false], ['T', 'T', 84, 0, false], ['Y', 'Y', 89, 0, false], ['U', 'U', 85, 0, false], 
                    ['I', 'I', 73, 0, false], ['O', 'O', 79, 0, false], ['P', 'P', 80, 0, false], ['{', '{', 219, 0, false], ['}', '}', 221, 0, false], ['|', '|', 220, 0, false],
                    ['A', 'A', 65, 0, true], ['S', 'S', 83, 0, false], ['D', 'D', 68, 0, false], ['F', 'F', 70, 0, false], ['G', 'G', 71, 0, false], ['H', 'H', 72, 0, false], ['J', 'J', 74, 0, false], 
                    ['K', 'K', 75, 0, false], ['L', 'L', 76, 0, false], [':', ':', 186, 0, false], ['"', '"', 222, 0, false], ['Enter', '13', 13, 3, false],
                    ['Shift', '16', 16, 2, true], ['Z', 'Z', 90, 0, false], ['X', 'X', 88, 0, false], ['C', 'C', 67, 0, false], ['V', 'V', 86, 0, false], ['B', 'B', 66, 0, false], ['N', 'N', 78, 0, false], 
                    ['M', 'M', 77, 0, false], ['<', '<', 188, 0, false], ['>', '>', 190, 0, false], ['?', '?', 191, 0, false], ['Shift', '16', 16, 2, false],
                    ['Bksp', '8', 8, 3, true], ['Space', '32', 32, 12, false], ['Clear', '46', 46, 3, false], ['Cancel', '27', 27, 3, false]  
                ]
            ]
        ]
    };


        var donateKeyboardLayout = {
        'layout': [
            // alphanumeric keyboard type
            // text displayed on keyboard button, keyboard value, keycode, column span, new row
            [
                [
                    ['1', '1', 49, 0, true], ['2', '2', 50, 0, false], ['3', '3', 51, 0, false], ['4', '4', 52, 0, false], ['5', '5', 53, 0, false], ['6', '6', 54, 0, false], 
                    ['7', '7', 55, 0, false], ['8', '8', 56, 0, false], ['9', '9', 57, 0, false], ['0', '0', 48, 0, false], ['Bksp', '8', 8, 3, true]
                ]
            ]
        ]
    };

    var activeInput = {
        'htmlElem': '',
        'initValue': '',
        'keyboardLayout': keyboardLayout,
        'donateKeyboardLayout': donateKeyboardLayout,
        'keyboardType': '0',
        'keyboardSet': 0,
        'dataType': 'string',
        'isMoney': false,
        'thousandsSep': ',',
        'disableKeyboardKey': false
    };

    /*
     * initialize keyboard
     * @param {type} settings
     */
    $.fn.initKeypad = function(settings){
        //$.extend(activeInput, settings);

        $(this).click(function(e){
            $("#jQKeyboardContainer").remove();
            activateKeypad(e.target);
        });
    };
    
    /*
     * create keyboard container and keyboard button
     * @param {DOM object} targetInput
     */
    function activateKeypad(targetInput){
        if($('div.jQKeyboardContainer').length === 0)
        {
            activeInput.htmlElem = $(targetInput);
            activeInput.initValue = $(targetInput).val();

            $(activeInput.htmlElem).addClass('focus');
            createKeypadContainer();
            createKeypad(0);
        }
    }
    
    /*
     * create keyboard container
     */
    function createKeypadContainer(){
        var container = document.createElement('div');
        container.setAttribute('class', 'jQKeyboardContainer');
        container.setAttribute('id', 'jQKeyboardContainer');
        container.setAttribute('name', 'keyboardContainer' + activeInput.keyboardType);
        
        $('body').append(container);
    }
    
    /*
     * create keyboard
     * @param {Number} set
     */
    function createKeypad(set){
        $('#jQKeyboardContainer').empty();
        if ( $('input.donateinput').is(':focus') ) {
            var layout = activeInput.donateKeyboardLayout.layout[activeInput.keyboardType][set];
        }else {
            var layout = activeInput.keyboardLayout.layout[activeInput.keyboardType][set];
        }
        

        for(var i = 0; i < layout.length; i++){

            if(layout[i][4]){
                var row = document.createElement('div');
                row.setAttribute('class', 'jQKeyboardRow');
                row.setAttribute('name', 'jQKeyboardRow');
                $('#jQKeyboardContainer').append(row);
            }

            var button = document.createElement('button');
            button.setAttribute('type', 'button');
            button.setAttribute('name', 'key' + layout[i][2]);
            button.setAttribute('id', 'key' + layout[i][2]);
            button.setAttribute('class', 'jQKeyboardBtn' + ' ui-button-colspan-' + layout[i][3]);
            button.setAttribute('data-text', layout[i][0]);
            button.setAttribute('data-value', layout[i][1]);
            button.innerHTML = layout[i][0];
            
            $(button).click(function(e){
               getKeyPressedValue(e.target); 
            });

            $(row).append(button);
        }
    }
    /*
     * remove keyboard from kepad container
     */
    function removeKeypad(){
        $('#jQKeyboardContainer').remove();
        $(activeInput.htmlElem).removeClass('focus');
    }
    
    /*
     * handle key pressed
     * @param {DOM object} clickedBtn
     */
    function getKeyPressedValue(clickedBtn){
        var caretPos = getCaretPosition(activeInput.htmlElem);
        var keyCode = parseInt($(clickedBtn).attr('name').replace('key', ''));
        
        var currentValue = $(activeInput.htmlElem).val();
        var newVal = currentValue;
        var closeKeypad = false;
        
        /*
         * TODO
        if(activeInput.isMoney && activeInput.thousandsSep !== ''){
            stripMoney(currentValue, activeInput.thousandsSep);
        }
        */
        
        switch(keyCode){
            case 8:     // backspace key
                newVal = onDeleteKeyPressed(currentValue, caretPos);
              caretPos--;
                break;
            case 13:    // enter key
                closeKeypad = onEnterKeyPressed();
                break;
            case 16:    // shift key
                onShiftKeyPressed();
                break;
            case 27:    // cancel key
                closeKeypad = true;
                newVal = onCancelKeyPressed(activeInput.initValue);
                break;
            case 32:    // space key
                newVal = onSpaceKeyPressed(currentValue, caretPos);
                caretPos++;
    break;
            case 46:    // clear key
                newVal = onClearKeyPressed();
                break;
            case 190:   // dot key
                newVal = onDotKeyPressed(currentValue, $(clickedBtn), caretPos);
                caretPos++;
                break;
            default:    // alpha or numeric key
                newVal = onAlphaNumericKeyPressed(currentValue, $(clickedBtn), caretPos);
                caretPos++;
                break;
        }
        
        // update new value and set caret position
        $(activeInput.htmlElem).val(newVal);
        setCaretPosition(activeInput.htmlElem, caretPos);

        if(closeKeypad){
            removeKeypad();
            $(activeInput.htmlElem).blur();
        }
    }
    
    /*
     * handle delete key pressed
     * @param value 
     * @param inputType
     */
    function onDeleteKeyPressed(value, caretPos){
        var result = value.split('');
        
        if(result.length > 1){
            result.splice((caretPos - 1), 1);
            return result.join('');
        }
    }
    
    /*
     * handle shift key pressed
     * update keyboard layout and shift key color according to current keyboard set
     */
    function onShiftKeyPressed(){
        var keyboardSet = activeInput.keyboardSet === 0 ? 1 : 0;
        activeInput.keyboardSet = keyboardSet;

        createKeypad(keyboardSet);
        
        if(keyboardSet === 1){
            $('button[name="key16"').addClass('shift-active');
        }else{
            $('button[name="key16"').removeClass('shift-active');
        }
    }
    
    /*
     * handle space key pressed
     * add a space to current value
     * @param {String} curVal
     * @returns {String}
     */
    function onSpaceKeyPressed(currentValue, caretPos){
        return insertValueToString(currentValue, ' ', caretPos);
    }
    
    /*
     * handle cancel key pressed
     * revert to original value and close key pad
     * @param {String} initValue
     * @returns {String}
     */
    function onCancelKeyPressed(initValue){
        return initValue;
    }
    
    /*
     * handle enter key pressed value
     * TODO: need to check min max value
     * @returns {Boolean}
     */
    function onEnterKeyPressed(){
        return true;
    }
    
    /*
     * handle clear key pressed
     * clear text field value
     * @returns {String}
     */
    function onClearKeyPressed(){
        return '';
    }
    
    /*
     * handle dot key pressed
     * @param {String} currentVal
     * @param {DOM object} keyObj
     * @returns {String}
     */
    function onDotKeyPressed(currentValue, keyElement, caretPos){
        return insertValueToString(currentValue, keyElement.attr('data-value'), caretPos);
    }
    
    /*
     * handle all alpha numeric keys pressed
     * @param {String} currentVal
     * @param {DOM object} keyObj
     * @returns {String}
     */
    function onAlphaNumericKeyPressed(currentValue, keyElement, caretPos){
        return insertValueToString(currentValue, keyElement.attr('data-value'), caretPos);
    }
    
    /*
     * insert new value to a string at specified position
     * @param {String} currentValue
     * @param {String} newValue
     * @param {Number} pos
     * @returns {String}
     */
    function insertValueToString(currentValue, newValue, pos){
        var result = currentValue.split('');
        result.splice(pos, 0, newValue);
        
        return result.join('');
    }
    
   /*
    * get caret position
    * @param {DOM object} elem
    * @return {Number}
    */
    function getCaretPosition(elem){
        var input = $(elem).get(0);

        if('selectionStart' in input){    // Standard-compliant browsers
            return input.selectionStart;
        } else if(document.selection){    // IE
            input.focus();
            
            var sel = document.selection.createRange();
            var selLen = document.selection.createRange().text.length;
            
            sel.moveStart('character', -input.value.length);
            return sel.text.length - selLen;
        }
    }
    
    /*
     * set caret position
     * @param {DOM object} elem
     * @param {Number} pos
     */
    function setCaretPosition(elem, pos){
        var input = $(elem).get(0);
        
        if(input !== null) {
            if(input.createTextRange){
                var range = elem.createTextRange();
                range.move('character', pos);
                range.select();
            }else{
                input.focus();
                input.setSelectionRange(pos, pos);
            }
        }
    }
})(jQuery);




});





    
