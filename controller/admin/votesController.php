<?php

class votesController extends adminController {
	
	function __construct(){
		parent::__construct("Vote");
	}
	
	function index(Array $params = []){
		// $this->_viewData->hasCreateBtn = true;		

		parent::index($params);
	}  

	public function exportBoothVotes(Array $params = []){
        header('Content-Type: text/csv; charset=utf-8');
        header('Content-Disposition: attachment; filename=citi_expo_event_booth_voting.csv');
        
        $sql="SELECT * FROM votes WHERE active=1";

        $votes = \Model\Vote::getList(['sql'=>$sql]);
        $output = fopen('php://output', 'w');
        $t=array("No.",'Favourite Booth');
        fputcsv($output, $t);
        $row ='';

        foreach($votes as $key=>$vote) {
            $row = array($key+1,$vote->fav_booth);
            fputcsv($output, $row);  
        }
    }
}