<?php

class aboutlabgrowndiamondsController extends siteController
{

    public function index(Array $params = [])
    {

        $this->configs['Meta Title'] = "Lab Grown Diamonds Info";
        $this->loadView($this->viewData);
    }

}