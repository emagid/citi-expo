<?php

use Twilio\Rest\Client;

class homeController extends siteController {
        function __construct(){
        parent::__construct();
    }    
    public function index(Array $params = []){


        $this->viewData->banners = \Model\Banner::getList(['where'=>"active='1'"]);
        $this->viewData->mainBanner = \Model\Banner::getList(['where'=>"active = 1 and featured_id = 0", 'orderBy'=>"banner_order asc"]);
        $this->viewData->jewelry = \Model\Jewelry::getList(['where' => "active = 1 and featured = 1 and discount != 0", 'orderBy' => "RANDOM()", 'limit' => 2]);

        $this->viewData->diamond = \Model\Product::getItem(null, ['where' => "active = 1 and featured = 1 and discount != 0 and quantity > 0", 'orderBy' => "RANDOM()", 'limit' => 1]);
        $this->viewData->ring = \Model\Ring::getItem(null, ['where' => "active = 1 and featured = 1 and discount != 0", 'orderBy' => "RANDOM()", 'limit' => 1]);
        
        $this->loadView($this->viewData);
    }

    function photobooth(Array $params = []){
        $this->loadView($this->viewData);
    }

    function answer(Array $params = []){
        $this->loadView($this->viewData);
    }

    function cyber(Array $params = []){
        $this->loadView($this->viewData);
    }


    function products(Array $params = []){
        $this->loadView($this->viewData);
    }

    function security(Array $params = []){
        $this->loadView($this->viewData);
    }


    function trivia(Array $params = []){
        $this->loadView($this->viewData);
    }

    function healthcare(Array $params = []){
        $this->loadView($this->viewData);
    }

    function privatecloud(Array $params = []){
        $this->loadView($this->viewData);
    }

    function hybrid(Array $params = []){
        $this->loadView($this->viewData);
    }

    function network(Array $params = []){
        $this->loadView($this->viewData);
    }

    function management(Array $params = []){
        $this->loadView($this->viewData);
    }

    function disaster(Array $params = []){
        $this->loadView($this->viewData);
    }

    function ransomeware(Array $params = []){
        $this->loadView($this->viewData);
    }

    function ny1(Array $params = []){
        $this->loadView($this->viewData);
    }

    function survey(Array $params = [])
    {
        $questions = \Model\Question::getList(['orderBy'=>'display_order ASC']);
        foreach ($questions as $i=> $question){
            $questions[$i]->answers = $question->get_answers();
        }
        $this->viewData->questions = $questions;

        $this->loadView($this->viewData);
    }

    function trivia_entry_post(){
        $response = ['status'=>false, 'msg'=>'failed'];
        header('Content-Type: application/json');

        if(isset($_POST['email']) && isset($_POST['name']) && $_POST['email'] != '' && $_POST['name'] != ''){
            $email = $_POST['email'];            
            if(\Model\Trivia_Entry::getItem(null,['where'=>"email = '$email'"]) == ''){
                $new_entry = new \Model\Trivia_Entry($_POST);
                $new_entry->save();
                if($new_entry->save()){
                    $response['status'] = true;
                    $response['msg'] = "success";
                }
            }
            else{
                $response['status'] = false;
                $response['msg'] ="failed";
            }
            echo json_encode($response);
            // if(\Model\Trivia_Entry::getItem(null,['where'=>"email = $email"]) != '')
        }
        
    }

    function selfie(Array $params = []){
        $this->viewData->event_pictures = \Model\Snapshot_Contact::getList(['orderBy'=>"id DESC"]);
        $this->loadView($this->viewData);
    }

    public function selfie_post(){
        $response = ['status'=>false, 'msg'=>'failed to Send'];
        $obj = \Model\Contact::loadFromPost();
        if(count($_POST['email']) > 0){
            $obj->email = $_POST['email'][0];
        } else {
            $obj->email = '';
        }
        if(count($_POST['phone']) > 0){
            $obj->phone = $_POST['phone'][0];
        } else {
            $obj->phone = '';
        }
        if($obj->save()){
            $imgs = [];
            $gif = [];
            $email_image = null;
            if(isset($_POST['image'])){
                $img = $_POST['image'];
                $image = \Model\Snapshot_Contact::getItem($img);
                $image->contact_id = $obj->id;
                if($image->save()){
                    $response['image']=$image;
                    $email_image = 'https://'.$_SERVER['SERVER_NAME'].$image->get_image_url();
                }
            } else if(isset($_POST['gif'])){
                $_gif = $_POST['gif'];
                $gif = \Model\Gif::getItem($_gif);
                $gif->contact_id = $obj->id;
                $gif->image = str_replace('\\','/',$gif->image);
                if($gif->save()){
                    $response['gif'] = $gif;
                    $email_image = 'https://'.$_SERVER['SERVER_NAME'].$gif->get_image_url();
                }
            }

            $email = new \Email\MailMaster();
            $sid = TWILIO_SID;
            $token = TWILIO_TOKEN;
            $client = new Client($sid, $token);

            $email_responses = [];
            $phone_responses = [];

            foreach ($_POST['email'] as $_email){
                if($_email == '') continue;
                if($email_image == null)
                    $eimage = '<p>Thanks for attending!</p>';
                else
                    $eimage = $email_image;
                $mergeTags = [
                    'CONTENT'=>"<img style='max-width: 728px; width: 100%;'src='$eimage'>"
                ];
                $email_responses[$_email] = $email->setTo(['email' => $_email, 'name' => ucwords($obj->name), 'type' => 'to'])->setSubject('Thank You!')->setTemplate('citi-expo-photobooth')->setMergeTags($mergeTags)->send();
            }

            foreach ($_POST['phone'] as $phone){

                if($phone == '') continue;
                try{
                    $phone_number = $client->lookups->v1->phoneNumbers($phone)
                        ->fetch(array(
                            'addons' => "whitepages_pro_caller_id"
                            )
                        );

                    if($phone_number->addOns['status'] == 'successful'){
                        $phone_responses[$phone] = $client->messages
                        ->create(
                            $phone,
                            array(
                                "from" => TWILIO_NUMBER,
                                "body" => "Here's your picture!! ". $email_image,
                            )
                        );
                    }
                    else{
                        $n = new \Notification\ErrorHandler("SOmething went wrong..Please Try again!!");
                            $_SESSION['notification'] = serialize($n);
                    }

                }catch(Exception $e){
                    // if($e->getStatusCode() == 404) {
                    //     $n = new \Notification\ErrorHandler("<p class='error'>Please Enter Valid Phone Number</p>");
                    //     $_SESSION['notification'] = serialize($n);
                    // }
                    // else{
                    //     $n = new \Notification\ErrorHandler("<p class='error'>Something went wrong...Please try agin with valid phone number!!</p>");
                    //     $_SESSION['notification'] = serialize($n);
                    // }
                }
            }

            $response['phones'] = $phone_responses;
            $response['emails'] = $email_responses;
            $response['status'] = true;
            $response['msg'] = 'Success';
            $response['contact'] = $obj;
        }
        // redirect("/");
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    function booth_post(){
        $resp['status'] = false;
        $vote = new \Model\Vote();
        $vote->fav_booth = $_POST['vote_answer'];
        if($vote->save()){
            $resp['status'] = true;
        }
        $this->toJson($resp);
    }

}